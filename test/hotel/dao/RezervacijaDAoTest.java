package hotel.dao;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hotel.model.Rezervacija;
import hotel.model.Soba;
import hotel.util.PomocnaKlasa;


public class RezervacijaDAoTest {

	Connection conn;
	@Before
	public void setUp() throws Exception {
	
			try {
				// ucitavanje MySQL drajvera
				Class.forName("com.mysql.jdbc.Driver");
				// otvaranje konekcije
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/hotel?useSSL=false", 
						"root", 
						"root");
			} catch (Exception ex) {
				System.out.println("Neuspela konekcija na bazu!");
				ex.printStackTrace();
				// kraj aplikacije
				System.exit(0);
			}
	}

	@After
	public void tearDown() throws Exception {
		
		Soba soba = SobaDAO.getSobaByTip(conn, "TestSoba");
			if(soba != null) {
				SobaDAO.deleteSobaByID(conn, soba.getId());
			}
		Rezervacija rez = RezervacijaDAO.getRezByGost(conn, "gostNovi");
		if(rez != null) {
			RezervacijaDAO.deleteRezByID(conn, rez.getId());
		}
		conn.close();
	}

	@Test
	public void test() throws ParseException {
	
			Soba soba = new Soba("TestSoba", 24, 60000.00);

			assertTrue(SobaDAO.insertSoba(conn, soba));
			soba = SobaDAO.getSobaByTip(conn, "TestSoba");
			
			Rezervacija rez = new Rezervacija(soba, new Date() , new Date(), "gostNovi");

			assertTrue(RezervacijaDAO.insertRez(conn, rez));
			rez = RezervacijaDAO.getRezByGost(conn, "gostNovi");
			
			rez.setUlazak(PomocnaKlasa.sdf.parse("2016-11-18 07:00:00"));
			rez.setIzlazak(PomocnaKlasa.sdf.parse("2016-11-25 07:00:00"));
			RezervacijaDAO.updateRezervacija(conn, rez);
			Rezervacija rez2 = RezervacijaDAO.getRezById(conn, rez.getId());
			assertEquals(rez.getId(), rez2.getId());
			assertEquals(rez.getSoba(), rez2.getSoba());
			assertEquals( rez.getUlazak(), rez2.getUlazak());
			assertEquals(rez.getIzlazak(), rez2.getIzlazak());
			assertEquals(rez.getGost(), rez2.getGost());


		}


}
