package hotel.model;

public class Soba {

	protected int id;
	protected String tipSobe;
	protected int kreveti;
	protected double cenaNoc;
	
	public Soba(int id, String tipSobe, int kreveti, double cenaNoc) {
		super();
		this.id = id;
		this.tipSobe = tipSobe;
		this.kreveti = kreveti;
		this.cenaNoc = cenaNoc;
	}

	public Soba(String tipSobe, int kreveti, double cenaNoc) {
		super();
		this.tipSobe = tipSobe;
		this.kreveti = kreveti;
		this.cenaNoc = cenaNoc;
	}
	

	@Override
	public String toString() {
		return "Soba [id=" + id + ", tipSobe=" + tipSobe + ", kreveti=" + kreveti + ", cenaNoc=" + cenaNoc + "]";
	}

	public Soba() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipSobe() {
		return tipSobe;
	}

	public void setTipSobe(String tipSobe) {
		this.tipSobe = tipSobe;
	}

	public int getKreveti() {
		return kreveti;
	}

	public void setKreveti(int kreveti) {
		this.kreveti = kreveti;
	}

	public double getCenaNoc() {
		return cenaNoc;
	}

	public void setCenaNoc(double cenaNoc) {
		this.cenaNoc = cenaNoc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(cenaNoc);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + id;
		result = prime * result + kreveti;
		result = prime * result + ((tipSobe == null) ? 0 : tipSobe.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Soba other = (Soba) obj;
		if (id != other.id)
			return false;
		return true;
	}

	
	
	
}
