package hotel.model;

import java.util.Date;

import hotel.util.PomocnaKlasa;



public class Rezervacija {

	protected int id;
	protected Soba soba;
	protected Date ulazak;
	protected Date izlazak;
	protected String gost;
	public Rezervacija(int id, Soba soba, Date ulazak, Date izlazak, String gost) {
		super();
		this.id = id;
		this.soba = soba;
		this.ulazak = ulazak;
		this.izlazak = izlazak;
		this.gost = gost;
	}
	public Rezervacija(Soba soba, Date ulazak, Date izlazak, String gost) {
		super();
		this.soba = soba;
		this.ulazak = ulazak;
		this.izlazak = izlazak;
		this.gost = gost;
	}
	public Rezervacija() {
		super();
	}
	@Override
	public String toString() {
		return "Rezervacija [id=" + id + ", soba=" + soba + ", ulazak=" + ulazak + ", izlazak=" + izlazak + ", gost="
				+ gost + "]";
	}
	public String toStringFormat() {
		return String.format("|%4d|%20s|%20s|%-20s|%7d|%27.2f|\n", id, PomocnaKlasa.sdf.format(ulazak),
				PomocnaKlasa.sdf.format(izlazak), gost, soba.getId(), nocenjaTotal());
	}
	
	public double nocenjaTotal() {
		long ul = ulazak.getTime();
		long iz = izlazak.getTime();
		long razlikaLongova = iz - ul;
		int danaUkupno = (int) (razlikaLongova/(1000*60*60*24));
		return (double) danaUkupno * soba.cenaNoc;
	}
	
	public boolean rezervacijaPripadaOpesegu(Date donjaGranica, Date gornjaGranica){
		boolean pripada = false;
		// |donjaGranica| -ulazak- -izlazak- |goranjGranica|
		pripada = donjaGranica.before(ulazak) && gornjaGranica.after(izlazak); //u celosti
		return pripada;
	}
	
	public boolean rezervacijaDelimicnoPripadaOpesegu(Date donjaGranica, Date gornjaGranica){
		boolean pripada = false;
		// |donjaGranica| -ulazak- |gornjaGranica|  -izlazak-
		pripada = pripada || (donjaGranica.before(ulazak) && ulazak.before(gornjaGranica));		
		// -ulazak- |donjaGranica|  -izlazak- |gornjaGranica|
		pripada = pripada || (donjaGranica.before(izlazak) && izlazak.before(gornjaGranica)); 
		return pripada;
	}
	
	public boolean rezervacijaObuhvataOpseg(Date donjaGranica, Date gornjaGranica){
		boolean pripada = false;
		// -ulazak- |donjaGranica| |goranjGranica| -izlazak- 
		pripada = ulazak.before(donjaGranica) && izlazak.after(gornjaGranica); //u celosti
		return pripada;
	}
	
	public boolean rezervacijaDelimicnoObuhvataOpeseg(Date donjaGranica, Date gornjaGranica){
		boolean pripada = false;
		// -ulazak- |donjaGranica|  -izlazak-
		pripada |= (ulazak.before(donjaGranica) && donjaGranica.before(izlazak));		
		// -ulazak- |gornjaGranica|  -izlazak-
		pripada |= (ulazak.before(gornjaGranica) && gornjaGranica.before(izlazak));
		return pripada;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Soba getSoba() {
		return soba;
	}
	public void setSoba(Soba soba) {
		this.soba = soba;
	}
	public Date getUlazak() {
		return ulazak;
	}
	public void setUlazak(Date ulazak) {
		this.ulazak = ulazak;
	}
	public Date getIzlazak() {
		return izlazak;
	}
	public void setIzlazak(Date izlazak) {
		this.izlazak = izlazak;
	}
	public String getGost() {
		return gost;
	}
	public void setGost(String gost) {
		this.gost = gost;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gost == null) ? 0 : gost.hashCode());
		result = prime * result + id;
		result = prime * result + ((izlazak == null) ? 0 : izlazak.hashCode());
		result = prime * result + ((soba == null) ? 0 : soba.hashCode());
		result = prime * result + ((ulazak == null) ? 0 : ulazak.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rezervacija other = (Rezervacija) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
