package hotel.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class PomocnaKlasa {

	static Scanner sc = new Scanner(System.in);
	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");// hh:mm:ss

	// citanje promenljive Date
	public static Date ocitajDatum() {
		String tekst = "";
		boolean formatirano = false;
		Date datum = null;
		while (!formatirano || tekst.equals("") || tekst == null) {
			System.out.println("Unesite vremenski period u formatu yyyy-MM-dd");
			tekst = sc.nextLine();
			try {
				datum = sdf.parse(tekst);
				formatirano = true;
			} catch (ParseException pe) {
				System.out.println("Niste uneli podatak validno. Pokusajte ponovo.");
			}
		}
		return datum;
	}
	//citanje promenljive String
	public static String ocitajTekst(){
		String tekst = "";
		while(tekst == null || tekst.equals(""))
			tekst = sc.nextLine();
		
		return tekst;
	}
		
	//citanje promenljive integer
	public static int ocitajCeoBroj(){
		while (sc.hasNextInt()==false) {
			System.out.println("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");
			sc.nextLine();
		}
		int ceoBroj = sc.nextInt();
		sc.nextLine(); //cisti sve sa ulaza sto nije broj ili ostatak teste posla broja
		return ceoBroj;
	}
		
	//citanje promenljive double
	public static float ocitajRealanBroj(){

		while (sc.hasNextFloat()==false) {
			System.out.println("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");
			sc.nextLine();
		}
		float realanBroj = sc.nextFloat();
		sc.nextLine(); //cisti sve sa ulaza sto nije broj ili ostatak teste posla broja
		return realanBroj;
	}
	
	public static double ocitajDouble(){

		while (sc.hasNextDouble()==false) {
			System.out.println("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");
			sc.nextLine();
		}
		double broj = sc.nextDouble();
		sc.nextLine(); //cisti sve sa ulaza sto nije broj ili ostatak teste posla broja
		return broj;
	}
		
	//citanje promenljive char
	public static char ocitajKarakter(){
		
		String text;
		while ( (text=sc.next())==null || text.length()!=1) {
			System.out.println("GRESKA - Pogresno unsesena vrednost za karakter, pokusajte ponovo: ");
			sc.nextLine();
		}
		char karakter = text.charAt(0);
		return karakter;
	}
	
	public static Date ocitajDatum2(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.");
		Date datum = null;
		while (datum == null) {
			try {
				datum = sdf.parse(sc.nextLine());
			}	catch(Exception e) {
				System.out.println("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");	
			}
		}
		
		//sc.nextLine(); //cisti sve sa ulaza sto nije broj ili ostatak teste posla broja
		return datum;
	}
	
	//citanje promenljive char
	public static char ocitajOdlukuOPotvrdi(String tekst){
		System.out.println("Da li zelite " + tekst + " [Y/N]:");
		char odluka = ' ';
		while( !(odluka == 'Y' || odluka == 'N' || odluka == 'y' || odluka == 'n') ){
			odluka = ocitajKarakter();
			if (!(odluka == 'Y' || odluka == 'N' || odluka == 'y' || odluka == 'n')) {
				System.out.println("Opcije su Y ili N");
			}
		}
		return odluka;
	}
	
	static boolean isInteger(String s){
		try {
			Integer.parseInt(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	static boolean isDouble(String s){
		try {
			Double.parseDouble(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	static boolean isBoolean(String s){
		try {
			Boolean.parseBoolean(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
