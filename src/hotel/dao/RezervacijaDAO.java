package hotel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hotel.model.Rezervacija;
import hotel.model.Soba;
import hotel.util.PomocnaKlasa;


public class RezervacijaDAO {
	
	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	public static List<Rezervacija> getAll(Connection conn) {
		List<Rezervacija> lista = new ArrayList<>();
		Statement stmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT * FROM rezervacije";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				int index = 1;
				int idR = rset.getInt(index++);
				int idS = rset.getInt(index++);
				Date ulazak = sdf.parse(rset.getString(index++));
				Date izlazak = sdf.parse(rset.getString(index++));
				String gost = rset.getString(index++);
				
				Soba soba = SobaDAO.getSobaByID(conn, idS);
				Rezervacija rez = new Rezervacija(idR, soba, ulazak, izlazak, gost);
				lista.add(rez);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return lista;
	}
	
	public static Rezervacija getRezById(Connection conn, int id) {
		Rezervacija rez = null;
		Statement stmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT * FROM rezervacije WHERE rezervacije.id= " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				int index = 1;
				int idR = rset.getInt(index++);
				int idS = rset.getInt(index++);
				Date ulazak = sdf.parse(rset.getString(index++));
				Date izlazak = sdf.parse(rset.getString(index++));
				String gost = rset.getString(index++);
				
				Soba soba = SobaDAO.getSobaByID(conn, idS);
				rez = new Rezervacija(idR, soba, ulazak, izlazak, gost);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return rez;
	}
	
	public static Rezervacija getRezByGost(Connection conn, String gost) {
		Rezervacija rez = null;
		Statement stmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT * FROM rezervacije WHERE rezervacije.gost= '" + gost + "'";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				int index = 1;
				int idR = rset.getInt(index++);
				int idS = rset.getInt(index++);
				Date ulazak = sdf.parse(rset.getString(index++));
				Date izlazak = sdf.parse(rset.getString(index++));
				String gostB = rset.getString(index++);
				
				Soba soba = SobaDAO.getSobaByID(conn, idS);
				rez = new Rezervacija(idR, soba, ulazak, izlazak, gostB);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return rez;
	}

	public static List<Rezervacija> getRezBySoba(Connection conn, Soba soba) {
		List<Rezervacija> lista = new ArrayList<>();
		Rezervacija rez = null;
		Statement stmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT * FROM rezervacije WHERE rezervacije.soba= " + soba.getId();
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				int index = 1;
				int idR = rset.getInt(index++);
				int idS = rset.getInt(index++);
				Date ulazak = sdf.parse(rset.getString(index++));
				Date izlazak = sdf.parse(rset.getString(index++));
				String gost = rset.getString(index++);
				Soba soba2 = SobaDAO.getSobaByID(conn, idS);
				rez = new Rezervacija(idR, soba2, ulazak, izlazak, gost);
				lista.add(rez);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return lista;
	}
	
	public static boolean updateRezervacija(Connection conn, Rezervacija rez) {
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE rezervacije SET  ulazak = ?, izlazak = ? WHERE id = ?";
			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, sdf.format(rez.getUlazak()));
			pstmt.setString(index++, sdf.format(rez.getIzlazak()));
			pstmt.setInt(index++, rez.getId());
			
			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}
	
	public static boolean deleteRezByID(Connection conn, int id) {
		Statement stmt = null;
		try {
			String update = "DELETE FROM rezervacije "+  "WHERE rezervacije.id= " +id;
			stmt = conn.createStatement();
			if(stmt.executeUpdate(update) == 1) {
				System.out.println("Uspesno brisanje rezervacije");
				return true;
			}
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return false;
	}

	public static boolean insertRez(Connection conn, Rezervacija rez) {
		boolean retVal = false;
		try {
			String update = "INSERT INTO rezervacije (soba, ulazak, izlazak, gost)"
					+ " VALUES (?, ?, ?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setInt(1, rez.getSoba().getId());
			pstmt.setString(2, sdf.format(rez.getUlazak()));
			pstmt.setString(3, sdf.format(rez.getIzlazak()));
			pstmt.setString(4, rez.getGost());
			 
			if(pstmt.executeUpdate() == 1)
				retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
}
