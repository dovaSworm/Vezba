package hotel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import hotel.model.Soba;


public class SobaDAO {
	
	public static List<Soba> getAllSobe(Connection conn){
		List<Soba> lista = new ArrayList<>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM sobe";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String tip = rset.getString(index++);
				int kreveti = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				Soba soba =  new Soba(id, tip, kreveti, cena);
				lista.add(soba);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return lista;
	}
	
	public static Soba getSobaByID(Connection conn, int id) {
		Soba soba = null;
		Statement stmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT * FROM sobe WHERE sobe.id= " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			while (rset.next()) {
				int index = 1;
				int idS = rset.getInt(index++);
				String tip = rset.getString(index++);
				int kreveti = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				soba = new Soba(idS, tip, kreveti, cena);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return soba;
	}
	
	public static Soba getSobaByTip(Connection conn, String tip) {
		Soba soba = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT * FROM sobe WHERE sobe.tip= ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, tip);
			rset = pstmt.executeQuery();
			while (rset.next()) {
				int index = 1;
				int idS = rset.getInt(index++);
				String tipB = rset.getString(index++);
				int kreveti = rset.getInt(index++);
				double cena = rset.getDouble(index++);
				soba = new Soba(idS, tipB, kreveti, cena);
			}
			rset.close();
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
			try {
				rset.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return soba;
	}
	
	public static boolean insertSoba(Connection conn, Soba soba) {
		boolean retVal = false;
		try {
			String update = "INSERT INTO sobe (tip, kreveti, cena)"
					+ " VALUES (?, ?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, soba.getTipSobe());
			pstmt.setInt(2, soba.getKreveti());
			pstmt.setDouble(3, soba.getCenaNoc());
			 
			if(pstmt.executeUpdate() == 1)
				retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean deleteSobaByID(Connection conn, int id) {
		Statement stmt = null;
		try {
			String update = "DELETE FROM sobe "+  "WHERE sobe.id= " + id;
			stmt = conn.createStatement();
			if(stmt.executeUpdate(update) == 1) {
				System.out.println("Uspesno brisanje rezervacije");
				return true;
			}
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}
		return false;
	}
}
