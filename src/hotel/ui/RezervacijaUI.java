package hotel.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import hotel.dao.RezervacijaDAO;
import hotel.model.Rezervacija;
import hotel.util.PomocnaKlasa;

public class RezervacijaUI {
	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public static void izmeniRezervaciju() {
		System.out.println("Unesite id rezervacije");
		int idR = PomocnaKlasa.ocitajCeoBroj();
		Rezervacija rez = RezervacijaDAO.getRezById(AplikacijaHotelUI.getConn(), idR);
		if (rez == null) {
			System.out.println("Pogresan id rezervacije");
			return;
		} else {
			System.out.println("Unesite novi datum ulaska u sobu");
			Date pocetak = PomocnaKlasa.ocitajDatum();
			System.out.println("Unesite novi datum izlaska iz sobe");
			Date kraj = PomocnaKlasa.ocitajDatum();
			if (pocetak.after(kraj)) {
				System.out.println("Uneti datum izlaska je pre ulaska");
				return;
			}
			List<Rezervacija> lista = RezervacijaDAO.getRezBySoba(AplikacijaHotelUI.getConn(), rez.getSoba());
			for (Rezervacija r : lista) {
				if ((pocetak.before(r.getUlazak()) && kraj.after(r.getIzlazak()))
						|| (pocetak.before(r.getUlazak()) && r.getUlazak().before(kraj))
						|| (pocetak.before(r.getIzlazak()) && r.getIzlazak().before(kraj))) {
					System.out.println("Soba je zauzeta u zadanom intervalu vremena");
					return;
				}
				rez.setUlazak(pocetak);
				rez.setIzlazak(kraj);
				RezervacijaDAO.updateRezervacija(AplikacijaHotelUI.getConn(), rez);
			}
		}
	}
	
	public static void ispisiRezUIntervaluVremena() {
		System.out.println("Unesite pocetni datum");
		Date pocetak = PomocnaKlasa.ocitajDatum();
		System.out.println("Unesite krajnji datum");
		Date kraj = PomocnaKlasa.ocitajDatum();
		if (pocetak.after(kraj)) {
			System.out.println("Uneti datum izlaska je pre ulaska");
			return;
		}
		List<Rezervacija> lista = RezervacijaDAO.getAll(AplikacijaHotelUI.getConn());
		for (Rezervacija r : lista) {
			if ((r.getUlazak().before(pocetak) && kraj.after(r.getIzlazak()))
					|| (r.getUlazak().after(pocetak) && pocetak.after(r.getIzlazak()))
					|| (r.getUlazak().before(kraj) && r.getIzlazak().before(kraj))) {
				System.out.println(r);
			}
		}
	}
}
