package hotel.ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hotel.dao.RezervacijaDAO;
import hotel.dao.SobaDAO;
import hotel.model.Rezervacija;
import hotel.model.Soba;
import hotel.util.PomocnaKlasa;


public class AplikacijaHotelUI {
	
	private static Connection conn;
	static {
		// otvaranje konekcije, jednom na pocetku aplikacije
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");
			// otvaranje konekcije
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/hotel?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			// kraj aplikacije
			System.exit(0);
		}
	}

	public static Connection getConn() {
		return conn;
	}
	
	private static void ispisiTekstOpcije() {	
		System.out.println("Svetska prvenstva - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Rad sa zicarama");
		System.out.println("\tOpcija broj 2 - Rad sa kartama");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");	
	}

	public static void meni() {
		int odluka = -1;
		while (odluka!= 0) {
			ispisiTekstOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
//				case 0:	
//					System.out.println("Izlaz iz programa");	
//					break;
//				case 1:
//					ZicaraUI.pozoviMeniZicara();
//					break;
//				case 2:
//					KartaUI.pozoviMeniKarta();
//					break;
				default:
					System.out.println("Nepostojeca komanda");
					break;
			}
		}
	}

	public static void main(String[] args) {
//		meni();
//SobaUI.ispisiSveSobe();
//		RezervacijaUI.ispisiRezUIntervaluVremena();
//		RezervacijaUI.izmeniRezervaciju();
		Rezervacija rez = RezervacijaDAO.getRezByGost(conn, "Petar Petrovic");
		System.out.println(rez.toStringFormat());
//		Rezervacija rez2 = RezervacijaDAO.getRezById(conn, 2);
//		System.out.println(rez2.toStringFormat());
//		Soba s = SobaDAO.getSobaByID(conn, 1);
//		System.out.println(s);
//		Soba soba = SobaDAO.getSobaByTip(conn, "Studio");
//		System.out.println(soba);
		
		
		
		try {
			rez.setUlazak(PomocnaKlasa.sdf.parse("2017-11-18 07:00:00"));
		} catch (ParseException e) {
			// TODO Auto-generated catch bloc
			e.printStackTrace();
		}
		rez.setIzlazak(new Date(new Date().getTime() + 33333333));
		RezervacijaDAO.updateRezervacija(conn, rez);
		Rezervacija rezerv = RezervacijaDAO.getRezByGost(conn, "Petar Petrovic");
		System.out.println(rezerv.toStringFormat());
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		System.out.print("Program izvrsen");
	}

}
