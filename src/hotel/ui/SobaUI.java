package hotel.ui;

import java.util.List;

import hotel.dao.SobaDAO;
import hotel.model.Soba;

public class SobaUI {

	public static void ispisiSveSobe() {
		List<Soba> lista = SobaDAO.getAllSobe(AplikacijaHotelUI.getConn());
		for(Soba soba : lista) {
			System.out.println(soba);
		}
	}
	
}
