DROP SCHEMA IF EXISTS hotel;
CREATE SCHEMA hotel DEFAULT CHARACTER SET utf8;
USE hotel;

CREATE TABLE sobe (
	id INT AUTO_INCREMENT,
	tip VARCHAR(20) NOT NULL, 
    kreveti INT NOT NULL,
    cena DOUBLE NOT NULL,
    
    PRIMARY KEY (id)
);

CREATE TABLE rezervacije (
	id INT AUTO_INCREMENT,
    soba int NOT NULL, 
    ulazak DATETIME NOT NULL, 
    izlazak datetime NOT NULL,
    gost VARCHAR(20) NOT NULL,
    
    PRIMARY KEY (id),
    FOREIGN KEY (soba) REFERENCES sobe(id)
);

INSERT INTO sobe (tip, kreveti, cena) VALUES ('Studio', 2, 2000.00), ('Suit', 1, 2500.00), ('Family room', 4, 3500.00), 
('Interconected rooms', 2, 2500.00), ('Interconected rooms', 2, 2500.00), ('Suit', 2, 3000.00);
INSERT INTO rezervacije (soba, ulazak, izlazak, gost) VALUES (6, '2017-11-01 12:00:00', '2017-11-10 10:00:00', 'Pero'), 
(3, '2017-11-05 13:00:00', '2017-11-10 08:00:00', 'Marko Marković'), (6, '2017-11-19 03:00:00', '2017-11-22 03:00:00', 'Jovan Jovanovi'), 
(3, '2017-11-10 12:30:00', '2017-11-20 07:00:00', 'Petar Petrović'), (4, '2017-11-10 12:00:00', '2017-12-02 08:00:00', 'Marko Marković'), 
(5, '2017-11-10 12:00:00', '2017-12-02 08:00:00', 'Marko Marković');